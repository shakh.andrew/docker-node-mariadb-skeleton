var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var useragent = require('express-useragent');
var passport = require('passport');
const expressip = require('express-ip');
const error = require('./app/utils/error');
const auth = require('./app/utils/auth');
const dotenv = require('dotenv');
dotenv.config({
  path: '.env'
});

// mongo connection
const connection = require('./app/configs/mongoose-connection');
connection.getMongoose();

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);
app.use(expressip().getIpInfoMiddleware);

app.use(logger('dev'));
app.use(useragent.express());
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// Passport:
app.use(passport.initialize());
passport.use(auth.jwtStrategy());

app.use('/', indexRouter);
app.use('/', require('./app/api/v1/index'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  error.show(err, res);
});

module.exports = app;

/**
 * TODO:
 * tests
 * registration
 * groups
 * redis
 */