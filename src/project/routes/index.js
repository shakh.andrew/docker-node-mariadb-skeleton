var express = require('express');
var user = require('../app/utils/useragent');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: '' });
});

module.exports = router;
