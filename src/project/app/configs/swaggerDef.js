const package = require('../../package');

module.exports = {
    info: {
        title: package.name,
        version: package.version,
        description: package.description,
    },
    basePath: '/',
    apis: [
        'app/api/v1/*/*.controller.js',
        'app/api/v1/*/*/*.controller.js'
    ],
    produces: [
        "application/json"
    ],
    authorizations: {},
    securityDefinitions: {
        bearer: {
            type: "apiKey",
            name: "Authorization",
            in: "header",
            bearerFormat: "JWT"
        }
    }
};