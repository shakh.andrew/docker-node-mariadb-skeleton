const mongooseConnection = require('mongoose');
const error = require('../utils/error');

mongooseConnection.set('debug', process.env.DEBUG === 'true');
mongooseConnection.Promise = global.Promise;

module.exports.connect = (cb) => {
    return mongooseConnection.connect(process.env.MONGO_DATABASE, {
        useNewUrlParser: true,
        user: process.env.MONGO_USER,
        pass: process.env.MONGO_PASS,
    }, cb).
        catch(error => console.log('ERROR MONGO CONNECTION: ', error));
};

module.exports.disconnect = () => {
    return mongooseConnection.disconnect();
};

module.exports.getMongoose = () => {
    this.disconnect();
    this.connect();
    return mongooseConnection;
}