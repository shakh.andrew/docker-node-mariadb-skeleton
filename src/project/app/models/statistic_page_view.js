module.exports = (sequelize, DataTypes) =>
{
    const StatisticPageView = sequelize.define('StatisticPageView', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            user_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id'
                },
                allowNull: true,
            },
            page_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'pages',
                    key: 'id'
                },
                allowNull: false,
            },
            browser: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            country: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            user_agent: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        {
            classMethods: {},
            tableName: 'statistic_page_views',
            underscored: true
        }
    );

    StatisticPageView.associate = (models) => {
        // 1 - 1
        StatisticPageView.belongsTo(models.User, { as: 'user', foreignKey : 'user_id'});
        StatisticPageView.belongsTo(models.Page, { as: 'page', foreignKey : 'page_id' });
    };

    return StatisticPageView;
};
