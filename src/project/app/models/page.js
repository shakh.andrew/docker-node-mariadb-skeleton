module.exports = (sequelize, DataTypes) =>
{
    const Page = sequelize.define('Page', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            body: {
                type: DataTypes.STRING,
                allowNull: false,
            }
        },
        {
            classMethods: {},
            tableName: 'pages',
            underscored: true
        }
    );

    Page.associate = (models) => {
        // 1 - n
        Page.hasMany(models.StatisticPageView, { as: 'view_page', foreignKey : 'page_id'});
    };

    return Page;
};
