module.exports = (sequelize, DataTypes) =>
{
    const User = sequelize.define('User', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            name: {
                type: new DataTypes.VIRTUAL(DataTypes.STRING, ['first_name', 'last_name']),
                get: function() {
                    return `${this.get('first_name')} ${this.get('last_name')}`
                }
            }
        },
        {
            classMethods: {},
            tableName: 'users',
            underscored: true
        }
    );

    User.associate = (models) => {
        // 1 - n
        User.hasMany(models.StatisticPageView, { as: 'view', foreignKey : 'user_id'});
    };

    return User;
};
