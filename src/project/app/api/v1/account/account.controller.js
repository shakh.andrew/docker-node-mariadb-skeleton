const express = require('express');
const accountMiddleware = require('./account.middleware');
const accountDao = require('./account.dao');
const response = require('../../../utils/response');

const router = express.Router();

/**
 * @swagger
 *
 * /account/login:
 *   post:
 *     description: Login and get token
 *     summary: get JWT token
 *     tags:
 *       - account
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: User email
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User password
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: inserted object
 */
router.post('/login',
    accountMiddleware.validateLogin,
    (req, res) => {

        accountDao.login(req.body.email, req.body.password)
            .then(item => response.parametrizedSuccess(res, { token: item } ))
            .catch(err => response.failed(res, err));
    });

module.exports = router;
