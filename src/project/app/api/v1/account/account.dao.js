const bcrypt = require('bcryptjs');
const auth = require('../../../utils/auth');
const models = require('../../../models');
const userDao = require('../user/user.dao');
const error = require('../../../utils/error');
const sequelize = require('sequelize');

/**
 * Login user
 *
 * @param email
 * @param password
 * @returns {Promise<T | never | never>}
 */
module.exports.login = (email, password) => {
    return userDao.getByEmail(email).then(user => {
        return bcrypt.compare(password, user.password).then(correct => {
            if (!correct) {
                throw error.forbidden('Incorrect email or password');
            }
            return auth.generateToken({ id: user.id });
        });
    });
};