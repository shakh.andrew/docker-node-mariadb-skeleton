const joi = require('@hapi/joi');
const error = require('../../../utils/error');

/**
 * Validate create
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.validateLogin = (req, res, next) => {
    const schema = joi.object().keys({
        email: joi.string().email().required(),
        password: joi.string().required(),
    });

    joi.validate(req.body, schema, (err, value) => {
        if (err) {
            const e = error.invalidJoi(err);
            return res.status(e.code).json(e)
        }
        return next();
    })
};