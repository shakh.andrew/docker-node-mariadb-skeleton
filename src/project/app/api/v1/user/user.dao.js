const models = require('../../../models');
const userModel = models.User;
const error = require('../../../utils/error');

/**
 * Get one user by ID
 *
 * @param id
 * @param transaction
 * @returns {Promise<T | never>}
 */
module.exports.getWithExeption = (id, transaction = null) => {
    return userModel.findOne({
        where: { id },
        transaction
    }).then(user => {
        if (user === null) {
            throw error.notFound('The user not found');
        }

        return user;
    });
};

/**
 * Get one user by email
 *
 * @param email
 * @param transaction
 * @returns {Promise<T | never>}
 */
module.exports.getByEmail = (email, transaction = null) => {
    return userModel.findOne({
        where: { email },
        transaction
    }).then(user => {
        if (user === null) {
            throw error.notFound('The user not found');
        }

        return user;
    });
};
