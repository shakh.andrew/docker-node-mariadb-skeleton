const user = require('../../../mongo-models/user');
const error = require('../../../utils/error');

/**
 * Get one user by ID
 *
 * @param id
 * @returns {Promise<T | never>}
 */
module.exports.getWithExeption = (id) => {
    return user.findById(id).then(user => {
        if (user === null) {
            throw error.notFound('The user not found');
        }

        return user;
    });
};

/**
 * Get one user by email
 *
 * @param email
 * @returns {Promise<T | never>}
 */
module.exports.getByEmail = (email) => {
    return user.findOne({ email }).then(user => {
        if (user === null) {
            throw error.notFound('The user not found');
        }

        return user;
    });
};
