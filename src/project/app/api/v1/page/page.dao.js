const models = require('../../../models');
const pageModel = models.Page;
const error = require('../../../utils/error');

/**
 * Get one page by ID
 *
 * @param id
 * @param transaction
 * @returns {Promise<T | never>}
 */
module.exports.getWithExeption = (id, transaction = null) => {
    return pageModel.findOne({
        where: { id },
        transaction
    }).then(page => {
        if (page === null) {
            throw error.notFound('The page not found');
        }

        return page;
    });
};
