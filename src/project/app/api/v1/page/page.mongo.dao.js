const page = require('../../../mongo-models/page');
const error = require('../../../utils/error');

/**
 * Get one page by ID
 *
 * @param id
 * @returns {Promise<T | never>}
 */
module.exports.getWithExeption = (id) => {
    return page.findById(id).then(page => {
        if (page === null) {
            throw error.notFound('The page not found');
        }

        return page;
    });
};
