const statisticPageClick = require('../../../../mongo-models/statistic_page_click');
const models = require('../../../../models');
const statisticPageViewModel = models.StatisticPageView;
const pageModel = models.Page;
const userModel = models.User;
const sequelize = require('sequelize');
const moment = require('moment');
const user = require('../../../../utils/useragent');
const Op = sequelize.Op;

/**
 * Add new statistic data
 *
 * @param req
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.track = (req) => {
    return user.parse(req).then(({country, browser}) => {

        return this.add(
            req.body.page_id,
            browser,
            country,
            req.useragent,
            req.body.user_id,
            req.body.timestamp
        );
    });
};

/**
 * Add new statistic data
 *
 * @param page_id
 * @param browser
 * @param country
 * @param user_agent
 * @param user_id
 * @param timestamp
 * @returns {insert}
 */
module.exports.add = (page_id, browser, country, user_agent, user_id = null, timestamp = null) => {
    const insert = {
        page_id,
        user_id,
        browser,
        country,
        user_agent: user_agent
    };

    if (timestamp) {
        insert.created_at = moment(timestamp, 'X').format('YYYY-MM-DD HH:mm:ss')
    }
    return statisticPageClick.create(insert);
};

/**
 * Get by pages
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByPages = (start = null, end = null) => {
    let match = {};
    if (end !== null && start !== null) {
        match = {
            created_at: {
                $gte: moment(start).startOf('day').toDate(),
                $lte: moment(end).endOf('day').toDate()
            }
        };
    }
    return statisticPageClick.aggregate(
        [
            {
                $match: match,
            },
            {
                $lookup: {
                    from: 'pages',
                    localField: 'page_id',
                    foreignField: '_id',
                    as: 'page_id',
                }
            },
            {
                $group: {
                    _id: '$page_id',
                    count: {$sum: 1},
                }
            },
            {
                $unwind: "$_id"
            },
            {
                $project: {
                    "_id._id": 1,
                    "_id.title": 1,
                    count: 1
                }
            }
        ]
    );
};

/**
 * Get by browsers
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByBrowsers = (start = null, end = null) => {
    let match = {};
    if (end !== null && start !== null) {
        match = {
            created_at: {
                $gte: moment(start).startOf('day').toDate(),
                $lte: moment(end).endOf('day').toDate()
            }
        };
    }
    return statisticPageClick.aggregate(
        [
            {
                $match: match,
            },
            {
                $group: {
                    _id: '$browser',
                    count: {$sum: 1},
                }
            },
        ]
    );
};

/**
 * Get by countries
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByCountries = (start = null, end = null) => {
    let match = {};
    if (end !== null && start !== null) {
        match = {
            created_at: {
                $gte: moment(start).startOf('day').toDate(),
                $lte: moment(end).endOf('day').toDate()
            }
        };
    }
    return statisticPageClick.aggregate(
        [
            {
                $match: match,
            },
            {
                $group: {
                    _id: '$country',
                    count: {$sum: 1},
                }
            },
        ]
    );
};

/**
 * Get by users
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByUsers = (start = null, end = null) => {
    let match = {};
    if (end !== null && start !== null) {
        match = {
            created_at: {
                $gte: moment(start).startOf('day').toDate(),
                $lte: moment(end).endOf('day').toDate()
            }
        };
    }
    return statisticPageClick.aggregate(
        [
            {
                $match: match,
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: 'user_id',
                }
            },
            {
                $group: {
                    _id: '$user_id',
                    count: {$sum: 1},
                }
            },
            {
                $unwind: "$_id"
            },
            {
                $project: {
                    "_id.first_name": 1,
                    "_id.last_name": 1,
                    "_id.email": 1,
                    count: 1
                }
            }
        ]
    );
};