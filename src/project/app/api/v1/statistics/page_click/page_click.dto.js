/**
 * Return items for pages report
 *
 * @param data
 * @returns {*}
 */
module.exports.byPages = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByPage(item));
    });
    return items;
};

function itemByPage(model) {
    const page = model._id;
    return {
        id: page._id ? page._id : null,
        title: page.title ? page.title : null,
        count: model.count ? model.count: 0,
    };
}

/**
 * Return items for browsers report
 *
 * @param data
 * @returns {*}
 */
module.exports.byBrowsers = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByBrowser(item));
    });
    return items;
};

function itemByBrowser(model) {
    return {
        browser: model._id ? model._id : null,
        count: model.count ? model.count: 0,
    };
}

/**
 * Return items for countries report
 *
 * @param data
 * @returns {*}
 */
module.exports.byCountries = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByCountry(item));
    });
    return items;
};

function itemByCountry(model) {
    return {
        country: model._id ? model._id : null,
        count: model.count ? model.count: 0,
    };
}

/**
 * Return items for users report
 *
 * @param data
 * @returns {*}
 */
module.exports.byUsers = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByUser(item));
    });
    return items;
};

function itemByUser(model) {
    const item = {
        count: model.count ? model.count: 0,
        user: {
            name: 'Unauthorized',
            email: null
        }
    };

    if (model._id) {
        const user = model._id;
        item.user.name = `${user.first_name} ${user.last_name}`;
        item.user.email = user.email ? user.email: null;
    }

    return item;
}