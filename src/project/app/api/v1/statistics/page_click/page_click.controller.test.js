const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../../../app');
const mocha = require('mocha');

chai.should();
chai.use(chaiHttp);

describe('PAGE CLICK API v1', () => {
    let accessToken, adminAccessToken;
    mocha.before((done) => {
        chai.request(server)
            .post('/account/login')
            .send({
                email: 'root@site.com',
                password: 'qwerty'
            })
            .end((err, res) => {
                chai.should().not.exist(err);
                res.should.have.status(200);
                res.body.should.be.an('object');
                res.body.should.have.property('token');
                accessToken = res.body.accessToken;
                adminAccessToken = res.body.accessToken;
                done()
            })
    });

    describe('GET /statistics/page-clicks/by-pages', () => {
        it('should list group pages with count', (done) => {
            chai.request(server)
                .get('/statistics/page-clicks/by-pages')
                .set('Authorization', adminAccessToken)
                .end((err, res) => {
                    chai.should().not.exist(err);
                    res.should.have.status(200);
                    res.body.should.be.an('array');
                    res.body.length.should.be.above(0);
                    done();
                });
        });
    });
});

