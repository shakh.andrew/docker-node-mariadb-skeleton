const express = require('express');
const pageClickDao = require('./page_click.dao');
const pageClickDto = require('./page_click.dto');
const pageViewMiddleware = require('./page_click.middleware');
const response = require('../../../../utils/response');
const auth = require('../../../../utils/auth');

const router = express.Router();

/**
 * @swagger
 *
 * /statistics/page-clicks:
 *   post:
 *     description: Tracking page click statistic
 *     summary: add click statistic data
 *     tags:
 *       - statistic_clicks
 *     security:
 *       - bearer: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user_id
 *         description: User ID.
 *         in: formData
 *         required: false
 *         type: string
 *       - name: page_id
 *         description: Page ID.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: timestamp
 *         description: Unix timestamp.
 *         in: formData
 *         required: false
 *         type: integer
 *     responses:
 *       200:
 *         description: inserted object
 */
router.post('/',
    auth.isAuthorized,
    pageViewMiddleware.validateCreate,
    pageViewMiddleware.validatePageExist,
    pageViewMiddleware.validateUserExist,
    (req, res) => {

        pageClickDao.track(req)
            .then(item => response.parametrizedSuccess(res, item))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-clicks/by-pages:
 *   get:
 *     description: Returns list of statistics data by pages
 *     summary: list of statistics data by pages
 *     tags:
 *       - statistic_clicks
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 format: int64
 *               title:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-pages',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageClickDao.getByPages(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageClickDto.byPages(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-clicks/by-browsers:
 *   get:
 *     description: Returns list of statistics data by browsers
 *     summary: list of statistics data by browsers
 *     tags:
 *       - statistic_clicks
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               browser:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-browsers',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageClickDao.getByBrowsers(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageClickDto.byBrowsers(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-clicks/by-countries:
 *   get:
 *     description: Returns list of statistics data by countries
 *     summary: list of statistics data by countries
 *     tags:
 *       - statistic_clicks
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               country:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-countries',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageClickDao.getByCountries(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageClickDto.byCountries(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-clicks/by-users:
 *   get:
 *     description: Returns list of statistics data by users
 *     summary: list of statistics data by users
 *     tags:
 *       - statistic_clicks
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               count:
 *                 type: integer
 *               user:
 *                 type: object
 *                 properties:
 *                   name:
 *                     type: string
 *                   email:
 *                     type: string
 */
router.get('/by-users',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageClickDao.getByUsers(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageClickDto.byUsers(items)))
            .catch(err => response.failed(res, err));
    });

module.exports = router;
