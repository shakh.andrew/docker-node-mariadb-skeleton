const express = require('express');
const router = express.Router();

router.use('/page-views', require('./page_view/page_view.controller'));
router.use('/page-clicks', require('./page_click/page_click.controller'));

module.exports = router;
