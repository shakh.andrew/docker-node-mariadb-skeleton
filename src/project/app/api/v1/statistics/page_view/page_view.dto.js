/**
 * Return items for pages report
 *
 * @param data
 * @returns {*}
 */
module.exports.byPages = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByPage(item));
    });
    return items;
};

function itemByPage(model) {
    return {
        id: model.id ? model.id : null,
        title: model.title ? model.title : null,
        count: model.dataValues.count ? model.dataValues.count: 0,
    };
}

/**
 * Return items for browsers report
 *
 * @param data
 * @returns {*}
 */
module.exports.byBrowsers = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByBrowser(item));
    });
    return items;
};

function itemByBrowser(model) {
    return {
        browser: model.browser ? model.browser : null,
        count: model.dataValues.count ? model.dataValues.count: 0,
    };
}

/**
 * Return items for countries report
 *
 * @param data
 * @returns {*}
 */
module.exports.byCountries = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByCountry(item));
    });
    return items;
};

function itemByCountry(model) {
    return {
        country: model.country ? model.country : null,
        count: model.dataValues.count ? model.dataValues.count: 0,
    };
}

/**
 * Return items for users report
 *
 * @param data
 * @returns {*}
 */
module.exports.byUsers = (data) => {
    let items = [];
    data.map(item => {
        items.push(itemByUser(item));
    });
    return items;
};

function itemByUser(model) {
    const item = {
        count: model.dataValues.count ? model.dataValues.count: 0,
        user: {
            name: 'Unauthorized',
            email: null
        }
    };

    if (model.user) {
        const user = model.user;
        item.user.name = user.name ? user.name: null;
        item.user.email = user.email ? user.email: null;
    }

    return item;
}