const models = require('../../../../models');
const statisticPageViewModel = models.StatisticPageView;
const pageModel = models.Page;
const userModel = models.User;
const sequelize = require('sequelize');
const moment = require('moment');
const user = require('../../../../utils/useragent');
const Op = sequelize.Op;

/**
 * Add new statistic data
 *
 * @param req
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.track = (req) => {
    return user.parse(req).then(({country, browser}) => {

        return this.add(
            req.body.page_id,
            browser,
            country,
            req.useragent,
            req.user.id,
            req.body.timestamp
        );
    });
};

/**
 * Add new statistic data
 *
 * @param page_id
 * @param browser
 * @param country
 * @param user_agent
 * @param user_id
 * @param timestamp
 * @returns {insert}
 */
module.exports.add = (page_id, browser, country, user_agent, user_id = null, timestamp = null) => {
    const insert = {
        page_id,
        user_id,
        browser,
        country,
        user_agent: JSON.stringify(user_agent)
    };

    if (timestamp) {
        insert.created_at = moment(timestamp, 'X').format('YYYY-MM-DD HH:mm:ss')
    }
    return statisticPageViewModel.create(insert);
};

/**
 * Get by pages
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByPages = (start = null, end = null) => {
    const config = {
        include: [
            {
                model: statisticPageViewModel,
                as: 'view_page',
                required: true,
                attributes: []
            }
        ],
        attributes: [
            'id', 'title', [sequelize.fn('COUNT', sequelize.col('view_page.id')), 'count']
        ],
        group: [
            ['id']
        ],
        order: [
            [sequelize.literal('`count`'), 'DESC']
        ]
    };

    if (start && end) {
        config.include[0].where = {
            created_at: {
                [Op.between]: [`${start} 00:00:00`, `${end} 23:59:59`]
            }
        }
    }

    return pageModel.findAll(config);
};

/**
 * Get by browsers
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByBrowsers = (start = null, end = null) => {
    const config = {
        attributes: {
            include: [
                [sequelize.fn('COUNT', sequelize.col('id')), 'count']
            ]
        },
        group: [
            ['browser']
        ],
        order: [
            [sequelize.literal('`count`'), 'DESC']
        ]
    };

    if (start && end) {
        config.where = {
            created_at: {
                [Op.between]: [`${start} 00:00:00`, `${end} 23:59:59`]
            }
        }
    }

    return statisticPageViewModel.findAll(config);
};

/**
 * Get by countries
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByCountries = (start = null, end = null) => {
    const config = {
        attributes: {
            include: [
                [sequelize.fn('COUNT', sequelize.col('id')), 'count']
            ]
        },
        group: [
            ['country']
        ],
        order: [
            [sequelize.literal('`count`'), 'DESC']
        ]
    };

    if (start && end) {
        config.where = {
            created_at: {
                [Op.between]: [`${start} 00:00:00`, `${end} 23:59:59`]
            }
        }
    }

    return statisticPageViewModel.findAll(config);
};

/**
 * Get by users
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByUsers = (start = null, end = null) => {
    const config = {
        include: [
            {
                model: userModel,
                as: 'user',
                required: false,
            }
        ],
        attributes: [
            'user_id', [sequelize.fn('COUNT', sequelize.col('StatisticPageView.id')), 'count']
        ],
        having: sequelize.where(sequelize.fn('COUNT', sequelize.col('StatisticPageView.id')), '>', 2),
        group: [
            ['user_id']
        ],
        order: [
            [sequelize.literal('`count`'), 'DESC']
        ]
    };

    if (start && end) {
        config.where = {
            created_at: {
                [Op.between]: [`${start} 00:00:00`, `${end} 23:59:59`]
            }
        }
    }

    return statisticPageViewModel.findAll(config);
};