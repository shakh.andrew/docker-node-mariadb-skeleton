const joi = require('@hapi/joi');
const error = require('../../../../utils/error');
const pageDao = require('../../page/page.dao');
const userDao = require('../../user/user.dao');

/**
 * Validate range of dates
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.validateDates = (req, res, next) => {
    const schema = joi.object().keys({
        start: joi.date().max(joi.ref('end')).optional(),
        end: joi.date().min(joi.ref('start')).max('now').optional(),
    });

    joi.validate(req.query, schema, (err, value) => {
        if (err) {
            let e = error.invalidJoi(err);
            return res.status(e.code).json(e)
        }
        return next();
    })
};

/**
 * Validate create
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.validateCreate = (req, res, next) => {
    const schema = joi.object().keys({
        page_id: joi.number().integer().required(),
        user_id: joi.number().integer().optional(),
        timestamp: joi.date().timestamp('unix').optional(),
    });

    joi.validate(req.body, schema, (err, value) => {
        if (err) {
            let e = error.invalidJoi(err);
            return res.status(e.code).json(e)
        }
        return next();
    })
};

/**
 * Validate page exist
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.validatePageExist = (req, res, next) => {
    pageDao.getWithExeption(req.body.page_id)
        .then(() => { return next() })
        .catch(e => { return res.status(e.code).json(e) });
};

/**
 * Validate user exist
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.validateUserExist = (req, res, next) => {
    if (req.body.user_id === undefined) {
        return next();
    }
    userDao.getWithExeption(req.body.user_id)
        .then(() => { return next() })
        .catch(e => { return res.status(e.code).json(e) });
};
