const express = require('express');
const pageViewDao = require('./page_view.dao');
const pageViewDto = require('./page_view.dto');
const pageViewMiddleware = require('./page_view.middleware');
const response = require('../../../../utils/response');
const auth = require('../../../../utils/auth');

const router = express.Router();

/**
 * @swagger
 *
 * /statistics/page-views:
 *   post:
 *     description: Tracking page view statistic
 *     summary: add view statistic data
 *     tags:
 *       - statistic
 *     security:
 *       - bearer: []
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: page_id
 *         description: Page ID.
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: timestamp
 *         description: Unix timestamp.
 *         in: formData
 *         required: false
 *         type: integer
 *     responses:
 *       200:
 *         description: inserted object
 */
router.post('/',
    auth.isAuthorized,
    pageViewMiddleware.validateCreate,
    pageViewMiddleware.validatePageExist,
    pageViewMiddleware.validateUserExist,
    (req, res) => {

        pageViewDao.track(req)
            .then(item => response.parametrizedSuccess(res, item))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-views/by-pages:
 *   get:
 *     description: Returns list of statistics data by pages
 *     summary: list of statistics data by pages
 *     tags:
 *       - statistic
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 format: int64
 *               title:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-pages',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageViewDao.getByPages(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageViewDto.byPages(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-views/by-browsers:
 *   get:
 *     description: Returns list of statistics data by browsers
 *     summary: list of statistics data by browsers
 *     tags:
 *       - statistic
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               browser:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-browsers',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageViewDao.getByBrowsers(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageViewDto.byBrowsers(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-views/by-countries:
 *   get:
 *     description: Returns list of statistics data by countries
 *     summary: list of statistics data by countries
 *     tags:
 *       - statistic
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               country:
 *                 type: string
 *               count:
 *                 type: integer
 */
router.get('/by-countries',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageViewDao.getByCountries(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageViewDto.byCountries(items)))
            .catch(err => response.failed(res, err));
    });

/**
 * @swagger
 * /statistics/page-views/by-users:
 *   get:
 *     description: Returns list of statistics data by users
 *     summary: list of statistics data by users
 *     tags:
 *       - statistic
 *     security:
 *       - bearer: []
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               count:
 *                 type: integer
 *               user:
 *                 type: object
 *                 properties:
 *                   name:
 *                     type: string
 *                   email:
 *                     type: string
 */
router.get('/by-users',
    auth.isAuthorized,
    pageViewMiddleware.validateDates,
    (req, res) => {

        pageViewDao.getByUsers(req.query.start, req.query.end)
            .then(items => response.parametrizedSuccess(res, pageViewDto.byUsers(items)))
            .catch(err => response.failed(res, err));
    });

module.exports = router;
