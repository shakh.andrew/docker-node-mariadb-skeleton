'use strict';
const DataTypes = require('sequelize/lib/data-types');
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('statistic_page_views', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            user_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'users',
                    key: 'id'
                },
                allowNull: true,
            },
            page_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: 'pages',
                    key: 'id'
                },
                allowNull: false,
            },
            browser: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            country: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            user_agent: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        }).then(() => {
            return queryInterface.addIndex('statistic_page_views', ['created_at', 'browser', 'country']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('statistic_page_views');
    }
};
