// const connection = require('../configs/mongoose-connection');
// connection.getMongoose();

const statisticPageClick = require('../mongo-models/statistic_page_click');
const page = require('../mongo-models/page');
const user = require('../mongo-models/user');
const rand = require('../utils/random');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        let allPages = await page.find();
        let allUsers = await user.find();
        const pages = allPages.map(i => i._id);
        const users = allUsers.map(i => i._id);
        const browsers = ['safari', 'chrome', 'firefox', 'edge', 'ie', 'opera'];
        const countries = ['USA', 'Ukraine', 'China', 'Poland', 'Spain', 'Italy', 'UAE'];
        const randomItems = [];

        for (let i = 0; i < 10000; i++) {
            randomItems.push({
                user_id: rand.any(users),
                page_id: rand.any(pages),
                browser: rand.any(browsers),
                country: rand.any(countries),
                created_at: rand.datetime('2019-01-01 00:00:00', '2019-07-27 23:59:59').format('YYYY-MM-DD HH:mm:ss')
            });
        }

        return statisticPageClick.insertMany(randomItems);
    },

    down: (queryInterface, Sequelize) => {

    }
};
