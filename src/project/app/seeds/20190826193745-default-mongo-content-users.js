const bcrypt = require('bcryptjs');

// const connection = require('../configs/mongoose-connection');
// connection.getMongoose();

const user = require('../mongo-models/user');

module.exports = {
    up: async (queryInterface, Sequelize) => {

        const salt = await bcrypt.genSalt(parseInt(process.env.SALT_ROUND));
        const pass = await bcrypt.hash('qwerty', salt);

        const data = [{
            first_name: 'Super',
            last_name: 'Admin',
            email: 'root@site.com',
            password: pass
        },{
            first_name: 'Tony',
            last_name: 'Stark',
            email: 'tony65@averengers.org',
            password: pass
        },{
            first_name: 'Torvalds',
            last_name: 'Linus',
            email: 'torvalds69@kernel.org',
            password: pass
        },{
            first_name: 'Tim',
            last_name: 'Berners-Lee',
            email: 'tim55@sw3.org',
            password: pass
        },{
            first_name: 'Steve',
            last_name: 'Jobs',
            email: 'steve55@apple.com',
            password: pass
        },{
            first_name: 'Harry',
            last_name: 'Potter',
            email: 'harry89@pottermore.com',
            password: pass
        },{
            first_name: 'Andrew',
            last_name: 'Shakh',
            email: 'shakh.andrew@gmail.com',
            password: pass
        }];

        return user.insertMany(data);
    },

    down: (queryInterface, Sequelize) => {

    }
};
