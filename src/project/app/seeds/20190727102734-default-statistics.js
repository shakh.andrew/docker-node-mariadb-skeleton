'use strict';
const rand = require('../utils/random');

module.exports = {
    up: (queryInterface, Sequelize) => {
        const browsers = ['safari', 'chrome', 'firefox', 'edge', 'ie', 'opera'];
        const countries = ['USA', 'Ukraine', 'China', 'Poland', 'Spain', 'Italy', 'UAE'];
        const randomItems = [];

        for (let i = 0; i < 10000; i++) {
            randomItems.push({
                user_id: rand.get(2, 6),
                page_id: rand.get(1, 5),
                browser: rand.any(browsers),
                country: rand.any(countries),
                created_at: rand.datetime('2019-01-01 00:00:00', '2019-07-27 23:59:59').format('YYYY-MM-DD HH:mm:ss')
            });
        }


        return queryInterface.bulkInsert('statistic_page_views', randomItems, {});
    },

    down: (queryInterface, Sequelize) => {

    }
};
