'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {

        return queryInterface.bulkInsert('pages', [{
            title: 'Page title #1',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #2',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #3',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #4',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #5',
            body: 'Test text test text test text test text test text test text'
        }], {});
    },

    down: (queryInterface, Sequelize) => {

    }
};
