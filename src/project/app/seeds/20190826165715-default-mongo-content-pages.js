const connection = require('../configs/mongoose-connection');
connection.getMongoose();

const page = require('../mongo-models/page');

module.exports = {
    up: (queryInterface, Sequelize) => {

        const data = [{
            title: 'Page title #1',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #2',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #3',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #4',
            body: 'Test text test text test text test text test text test text'
        },{
            title: 'Page title #5',
            body: 'Test text test text test text test text test text test text'
        }];

        return page.insertMany(data);
    },

    down: (queryInterface, Sequelize) => {

    }
};
