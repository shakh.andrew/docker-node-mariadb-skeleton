const jwt = require('jsonwebtoken');
const userDao = require("../api/v1/user/user.dao");
const passportJWT = require("passport-jwt");
const passport = require("passport");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const error = require('./error');

/**
 * Get JWT token
 *
 * @param data
 * @returns {*}
 */
module.exports.generateToken = (data) => {
    return jwt.sign( data, process.env.JWT_SECRET_PHRASE, { expiresIn: '24h' } );
};

/**
 * Verify token
 *
 * @param token
 * @returns {never}
 */
module.exports.verifyToken = (token) => {
    return Promise((resolve, reject) => {
        jwt.verify(token, process.env.JWT_SECRET_PHRASE, (err, decoded) => {
            if (err) {
                reject(error.forbidden('Token is not valid'));
            } else {
                resolve(decoded);
            }
        });
    });
};

/**
 * Passport strategy
 *
 * @returns {JwtStrategy}
 */
module.exports.jwtStrategy = () => {
    return new JWTStrategy({
            jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
            secretOrKey   : process.env.JWT_SECRET_PHRASE,
            jsonWebTokenOptions: { expiresIn: '24h' }
        },
        (jwtPayload, cb) => {
            checkUserById(jwtPayload.id, cb);
        }
    );
};

/**
 * Check user exist
 *
 * @param id
 * @param cb
 */
function checkUserById(id, cb) {
    userDao.getWithExeption(id).then(user => {
        return cb(null, user);
    }).catch(err => {
        return cb(err);
    });
}

/**
 * Middleware for authorized user
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
module.exports.isAuthorized = (req, res, next) => {
    return passport.authenticate(['jwt'], {session: false})(req, res, next);
};
