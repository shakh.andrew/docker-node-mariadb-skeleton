const {
    detect
} = require('detect-browser');
const geoLocation = require('../services/geo_location');

/**
 * Parse request object
 *
 * @param req
 * @returns {Promise<any>}
 */
module.exports.parse = req => {
    const ip = (req.headers["X-Forwarded-For"] ||
            req.headers["x-forwarded-for"] ||
            '').split(',')[0] ||
        req.client.remoteAddress;

    let browser = 'Unknown';
    const browserInfo = detect(req.headers['user-agent']);
    if (browserInfo && browserInfo.name) {
        browser = browserInfo.name;
    }

    return geoLocation.findCountry(ip).then(country => {
        return {
            ip,
            browser,
            country
        };
    });
};