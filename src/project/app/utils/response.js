const httpStatus = require('http-status-codes');
const error = require('./error');

module.exports.failed = (res, err) => {
    error.show(err, res);
};

module.exports.success = res => {
    res.status(httpStatus.OK).send();
};

module.exports.parametrizedSuccess = (res, items) => {
    res.status(httpStatus.OK).json(items);
};
