const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const statisticPageClick = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'Users' },
    page_id: { type: Schema.Types.ObjectId, ref: 'Pages' },
    browser: {type: String, default: ''},
    country: {type: String, default: ''},
    user_agent: {type: Schema.Types.Mixed, default: ''},
    created_at: {type: Date, default: Date.now},
}, {
    collection: 'statisticPageClicks',
    _id: true
});

module.exports = mongoose.model('statisticPageClick', statisticPageClick);