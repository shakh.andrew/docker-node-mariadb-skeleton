const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const user = new Schema({
    first_name: {type: String, trim: true, default: ''},
    last_name: {type: String, trim: true, default: ''},
    email: {
        type: String,
        trim: true,
        unique: true,
        lowercase: true,
        required: true,
        default: ''
    },
    password: {type: String, default: ''}
}, {
    collection: 'users',
    _id: true
});

user.virtual('name').get(function () {
    return this.first_name + ' ' + this.last_name;
});

module.exports = mongoose.model('user', user);