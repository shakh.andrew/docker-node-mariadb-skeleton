const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const page = new Schema({
    title: {type: String, trim: true, default: ''},
    body: {type: String, trim: true, default: ''},
}, {
    collection: 'pages',
    _id: true
});

module.exports = mongoose.model('page', page);