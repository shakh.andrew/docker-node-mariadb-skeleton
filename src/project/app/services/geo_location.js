const where = require('node-where');

/**
 * Get country bu IP
 *
 * @param ip
 * @returns {Promise<any>}
 */
module.exports.findCountry = ip => {
    return new Promise((resolve) => {

        where.is(ip, function(err, result) {
            let country = 'Unknown';
            if (result && result.get('country')) {
                country = result.get('country');
            }
            resolve(country);
        });
    });
};