### NODE MARIADB SKELETON 

This is development environment (NOT FOR PRODUCTION). You should install [Docker](https://www.docker.com/) and docker compose

### Quick start:
  - run install script `$ bash ./install.sh`
  - the site will be accessible via the link [http://localhost:3000/](http://localhost:3000/)
  - superadmin user loggin/password: `root@site.com` / `qwerty`
  - eat cookies with tea and enjoy JS magics :)

API documentation URL: [http://localhost:3000/api-docs](http://localhost:3000/api-docs)

___

There are extra commands:

**Start all containers**
```sh
$ docker-compose up --build -d
```

**Stop all containers**
```sh
$ docker-compose down
```

**Login into container**
```sh
$ docker exec -it <name> bash
```

**List all containers**
```sh
$ docker ps -a
```

**Remove all images**
```sh
$ docker rmi $(docker images -q)
```

### Database
All db files are stored on host machine (safe mode). So you import a dump only one time and the data will be saved even if you stop or remove container.

**Import MySQL dump**
```sh
$ docker exec -i db-server mysql -uuser -puser databasename < ./backup/dump.sql
```
**Import Mongo dump**
```sh
$ docker exec -i mongo mongoimport --username root --password qwerty --authenticationDatabase admin --db dbName --collection collectionName < backup/fileName.json
```

There is PhpMyAdmin container. URL for access to it: [http://localhost:8765](http://localhost:8765/)

There is MongoExpress container. URL for access to it: [http://localhost:8081](http://localhost:8081/)

**DB credentials:**
  - root / qwerty
  - user / user


### Structure of directories & files
```sh
.
├── backup/                      # backup directory 
│   └── ...                      # sql dumps
├── mariadb/                     # mariadb container directory
│   ├── configs/                 # config directory
│   │   └── my.conf              # main config of mariadb
│   └── data/                    # db data files (databases & tables)
│       └── ...                  # mariadb container stores there files
├── mongo/                       # mongo container directory
│   └── data/                    # db data files (databases & collections)
│       └── ...                  # mongo container stores there files
├── src/                         # server document root 
│   └── ...                      # project files
└── ...
```

### Structure of project (src/project/app)
This project uses multi level pattern of architecture with grouping logic entities.
```sh
.
├── api/                         # module structure of components
│   ├── *
│   │   ├── *.controller.js      # routes - list of API methods and documentation (witout business logic)
│   │   ├── *.dao.js             # there is business logic (it links models and controller)
│   │   ├── *.dto.js             # customization and access control data before show user
│   │   └── *.middleware.js      # validation
│   └── ...                  
├── congigs/                     # all configurations pf project
│   └── ...
├── migrations/                  # DB migration files
│   └── ...
├── models/                      # model of DB tables (without business logic. just object description and relations))
│   └── ...
├── seeds/                       # DB seeds
│   └── ...
├── services/                    # third part services
│   └── ...
├── utils/                       # helpers 
│   └── ...
└── ...
```