#!/bin/bash
cp src/project/.env.example src/project/.env
npm --prefix ./src/project install
docker-compose up --build -d
sleep 180 | echo "sleeping 180"
docker ps -a
docker exec -i web ./node_modules/.bin/sequelize db:migrate
docker exec -i web ./node_modules/.bin/sequelize db:seed:all
#docker exec -i web npm run test

echo "The end. Go to http://localhost:3000/";