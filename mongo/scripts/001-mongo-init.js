db.createUser(
    {
        user: "user-test",
        pwd: "password-test",
        roles: [
            {
                role: "readWrite",
                db: "testdb"
            }
        ]
    }
);

db.init.insert({ some_key: "some_value" });